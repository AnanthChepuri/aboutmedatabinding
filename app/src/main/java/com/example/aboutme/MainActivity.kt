package com.example.aboutme

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")

    private lateinit var binding: ActivityMainBinding
    private val myName: MyName = MyName("Aleks Haecky")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.myName= myName

        var descriptionTextView: TextView = findViewById(R.id.descriptionTextView)
        descriptionTextView.text =
            " I am Kennnydy John Victor passionate about my work, Because I love what I do, I have a steady source of motivation that drives me to do my best. \n In my last job, this passion led me to challenge myself daily and learn new skills that helped me to do better work. I am highly organised. \n I always take notes, and I use a series of tools to help myself stay on top of deadlines. I like to keep a clean workspace and create a logical filing method so I’m always able to find what I need.\n I find this increases efficiency and also helps the rest of the team stay on track. I'm a natural leader. I’ve eventually been promoted to a leadership role in almost every job because I like to help people. I find co-workers usually come to me with questions or concerns even when I’m not in a leadership role because if I don’t know the answer, I’ll at least point them in the right direction. In my last two roles, I was promoted to leadership positions after less than a year with the company.I’m a people person. I love meeting new people and learning about their lives and their backgrounds. I can almost always find common ground with strangers, and I like making people feel comfortable in my presence. I find this skill is especially helpful when kicking off projects with new clients."

        binding.doneButton.setOnClickListener {
            if (binding.nickNameEdittext.text.chars().count() >  1) {
                var str: String = "My nick name is "
                binding.apply {
                    myName?.nickName = str + binding.nickNameEdittext.text
                    binding.nickNameTextView.text = myName?.nickName.toString()
                    invalidateAll()
                }
            }
            binding.nickNameEdittext.visibility = GONE
            binding.doneButton.visibility = GONE
        }

        binding.editNickNameImage.setOnClickListener {
            binding.nickNameEdittext.visibility = View.VISIBLE
            binding.doneButton.visibility = View.VISIBLE
        }
    }
}